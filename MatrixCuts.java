package pa2;

import java.util.*;

/**
 * @author Nicholas Sagers
 */
public class MatrixCuts {
    // Can go down, left/down, or right/down
    public static ArrayList<Tuple> widthCut(int[][] M){ // Check logic
        // From top row to bottom, can go down/left down down/right by one p
        ArrayList<Tuple> result = new ArrayList<>();
        int widthCost = 0;

        // Min Path
        int numRows = M.length;
        int numCols = M[0].length;
        int[][] dp = new int[numRows][numCols];
       // Arrays.fill(dp, 0);

        // init dp with the same first row as M
        for (int i = 0; i < numCols; i++)
            dp[0][i] = M[0][i];

        for (int i = 1; i < numRows; i++) {
            for (int k = 0; k < numCols; k++) {
                {
                    if (k == 0)
                        dp[i][k] = Math.min((M[i][k] + dp[i - 1][k]), M[i][k] + dp[i - 1][k + 1]);
                    else if (k == numCols - 1)
                        dp[i][k] = Math.min((M[i][k] + dp[i - 1][k]), M[i][k] + dp[i - 1][k - 1]);
                    else {
                        dp[i][k] = Math.min((M[i][k] + dp[i - 1][k]), Math.min((M[i][k] + dp[i - 1][k - 1]), M[i][k] + dp[i - 1][k + 1]));
                    }
                }
            }
        }


        int tempMin = Integer.MAX_VALUE; // Find the smallest cost in bottom row
        int idx = 0;
        for (int i = 0; i < numCols; i++){
            if (dp[numRows-1][i] < tempMin) {
                tempMin = dp[numRows-1][i];
                idx = i;
            }
        }
        widthCost = tempMin;

        // Retrace back to the top row to find the correct path
        result.add(new Tuple(numRows-1, idx));

        for (int j = numRows - 2; j >= 0; j--){ // Start from the second to last row and go upwards
            if (idx == 0){ // First Column
                if (dp[j][idx+1] < dp[j][idx]){
                    idx++;
                }
                result.add(new Tuple(j, idx));
                continue;
            }

            else if (idx == numCols - 1){ // Last Column
                if (dp[j][idx-1] < dp[j][idx]){
                    idx--;
                }
                result.add(new Tuple(j, idx));
                continue;
            }

            else {
                if (dp[j][idx+1] < dp[j][idx] && dp[j][idx+1]  < dp[j][idx-1]) // up/right is min
                    idx++;
                else if (dp[j][idx-1] < dp[j][idx] && dp[j][idx-1] < dp[j][idx+1]){ // up/left is min
                    idx--;
                }
                else { // up is min
                    // Do nothing
                }
                result.add(new Tuple(j, idx));
                continue;
            }
        }

        Collections.reverse(result); // so the order of the result is top-down
        result.add(0, new Tuple(widthCost, -1)); // add the min-cost path sum to the front of the list
        return result;
    }

    public static ArrayList<Tuple> stitchCut(int[][] M){ // Can go right, down, or down/right
        ArrayList<Tuple> result = new ArrayList<>();
        int stitchCost = 0;
        int numRows = M.length;
        int numCols = M[0].length;
        int[][] dp = new int[numRows][numCols];

        for (int i = 0; i < numCols; i++) // Init the first row of dp
            dp[0][i] = M[0][i];

        for (int row = 1; row < numRows; row++){
            for (int col = 0; col < numCols; col++){

                if (col == 0){ // can only be accessed by going downwards
                    dp[row][col] = dp[row-1][col] + M[row][col];
                }

                else {
                    dp[row][col] = Math.min(Math.min((dp[row][col-1] + M[row][col]),
                            (dp[row-1][col-1] + M[row][col])),
                            (dp[row-1][col] + M[row][col]));
                    // Gets the min of itself + (directly above, one left, one up/left)
                }
            }
        }

        int tempMin = Integer.MAX_VALUE; int idx = 0; // Find smallest value in bottom row
        for (int i = 0; i < numCols; i++){
            if (dp[numRows-1][i] < tempMin){
                tempMin = dp[numRows-1][i];
                idx = i;
            }
        }
        stitchCost = tempMin;

        // Retrace back to the top row to find the correct path
        result.add(new Tuple(numRows-1, idx));
        int j = numRows -1;
        while (j > 0) {

            if (idx == 0){ // Can only go upwards if we hit the leftmost column
                for (int u = j-1; u >= 0; u--){
                    result.add(new Tuple(u, 0));
                }
                break;
            }

            if (dp[j][idx-1] <= dp[j-1][idx-1] && dp[j][idx-1] <= dp[j-1][idx]){ // Left smallest
                result.add(new Tuple(j, idx-1));
                idx--;
                continue;
            }

            else if (dp[j-1][idx-1] <= dp[j][idx-1] && dp[j-1][idx-1] <= dp[j-1][idx]){ // Up/left smallest
                result.add(new Tuple(j-1, idx-1));
                j--;
                idx--;
                continue;
            }

            else if (dp[j-1][idx] <= dp[j-1][idx-1] && dp[j-1][idx] <= dp[j][idx-1]){ // Up smallest
                result.add(new Tuple(j-1, idx));
                j--;
            }
        }


        Collections.reverse(result);
        result.add(0, new Tuple(stitchCost, -1));
        return result;
    }
}
