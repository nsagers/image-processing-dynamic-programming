package pa2;

import java.awt.Color;
import java.util.*;

/**
 * @author Nicholas Sagers
 */
public class ImageProcessor {
    public static Picture reduceWidth(int x, String inputImage){
        Picture result = new Picture(inputImage);
        if (x > result.width())
            throw new IllegalArgumentException("Cannot reduce width more than the total width of the image");
        for (int i = 0; i < x; i++){ // Calls the driver method x times
            result = _reduceWidth(x, result);
        }
        return result;
    }

    private static Picture _reduceWidth(int x, Picture thePicture){
        int[][] m = createImportance(thePicture); // Calculate pixel importance matrix
        List<Tuple> minWidthCut = MatrixCuts.widthCut(m); // Calculate minimum width cut

        minWidthCut.remove(0); // remove the first element "(X -1)"

        // Create a new Picture that is the same as the old picture expect with W-1 pixels
        Picture result = new Picture(thePicture.width() - 1, thePicture.height());
        for (int row = 0; row < thePicture.height(); row++){
            int idx = 0;
            for (int col = 0; col < thePicture.width(); col++){
                if (col != minWidthCut.get(row).getY()){
                    result.set(idx, row, thePicture.get(col, row));
                    idx++;
                }
            }
        }
        return result;
    }

    private static int[][] createImportance(Picture pic){
        int[][] result = new int[pic.height()][pic.width()];
        int rows = result.length;
        int cols = result[0].length;

        for (int row = 0; row < rows; row++){
            for (int col = 0; col < cols; col++){
                // x and y flipped
                if (col == 0)
                    result[row][col] = pixelDist((pic.get(col, row)), (pic.get(col+1, row)));
                else if (col == cols-1)
                    result[row][col] = pixelDist(pic.get(col, row), pic.get(col-1, row));
                else
                    result[row][col] = pixelDist(pic.get(col-1, row), pic.get(col+1, row));
            }
        }
        return result;
    }

    private static int pixelDist(Color c1, Color c2){
        int red = c1.getRed() - c2.getRed();
        int green = c1.getGreen() - c2.getGreen();
        int blue = c1.getBlue() - c2.getBlue();
        return (red*red) + (green*green) + (blue*blue);
    }
}
